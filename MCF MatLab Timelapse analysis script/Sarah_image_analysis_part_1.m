%% File_import
close all

Correct_data_loaded = input('Is correct peak data loaded? (0 or 1): ');

% Get directory, make it so the longest time is at the end of the file list
% and the flip function will put it first in analysis
Directory_images = dir;
Directory_images(1:2) = [];
Directory_images = flip(Directory_images);

ax = zeros(length(Directory_images),1);
bx = zeros(length(Directory_images),1);
ax(1) = 1;
bx(1) = 1;

for i = 2:1:length(Directory_images)
    if bx(i-1)*ax(i-1) < i
        if bx(i-1) < ax(i-1)
            bx(i) = bx(i-1)+1;
            ax(i) = ax(i-1);
        else
            bx(i) = bx(i-1);
            ax(i) = ax(i-1)+1;
        end
    else
        bx(i) = bx(i-1);
        ax(i) = ax(i-1);
    end
end

%% Pre-requisite stage

RGB = imread(Directory_images(1).name);
RGB_Red = RGB(:,:,1);
figure
imshow(RGB_Red)
title('Raw red image')

if Correct_data_loaded == 0
    % Manually adjust this!
    Row_obs = 1269;
end
hline(Row_obs,'r')

row_check = input('Is this row acceptable? (0 or 1): \n');
if row_check == 0
    Row_obs = input('Input row number of choice (use data cursor y value): \n');
end

Peak_and_Trough_Locations(1).Row_position = Row_obs;

figure
stairs(RGB_Red(Row_obs,:))
title('Instensity of horizontal slice of image')
xlabel('Column number')
ylabel('Intensity')

if Correct_data_loaded == 0
    Peak_and_Trough_Locations = struct('StripRange', cell(1,4));
end

if Correct_data_loaded == 0
    % Manual x range hardcode
    Peak_and_Trough_Locations(1).StripRange = 816:1064;
    Peak_and_Trough_Locations(2).StripRange = 1165:1397;
    Peak_and_Trough_Locations(3).StripRange = 1526:1760;
    Peak_and_Trough_Locations(4).StripRange = 1860:2087;
end

% Vertical lines on graph
vline(Peak_and_Trough_Locations(1).StripRange(1)); vline(Peak_and_Trough_Locations(1).StripRange(end));
vline(Peak_and_Trough_Locations(2).StripRange(1)); vline(Peak_and_Trough_Locations(2).StripRange(end));
vline(Peak_and_Trough_Locations(3).StripRange(1)); vline(Peak_and_Trough_Locations(3).StripRange(end));
vline(Peak_and_Trough_Locations(4).StripRange(1)); vline(Peak_and_Trough_Locations(4).StripRange(end));

Strip_search = input('Are the strip widths acceptable? (0 or 1): \n');
if Strip_search == 0
    disp('Select ranges using x values of datacursor\n')
    % Manual x range
    Peak_and_Trough_Locations(1).StripRange = input('Strip 1 x range e.g. (816:1064;) : \n');
    Peak_and_Trough_Locations(2).StripRange = input('Strip 2 x range e.g. (816:1064;) : \n');
    Peak_and_Trough_Locations(3).StripRange = input('Strip 3 x range e.g. (816:1064;) : \n');
    Peak_and_Trough_Locations(4).StripRange = input('Strip 4 x range e.g. (816:1064;) : \n');
end


%% Intensities of the four areas of interest (Non essential)

observeing_range_cols = Peak_and_Trough_Locations(1).StripRange(1):Peak_and_Trough_Locations(4).StripRange(end);

figure
for i = 1:length(Directory_images)
    RGB = imread(Directory_images(i).name);
    RGB_Red = RGB(:,:,1);
    subplot(ax(length(Directory_images)),bx(length(Directory_images)),i)
    stairs(1:length(observeing_range_cols),RGB_Red(Row_obs,observeing_range_cols))
    title([Directory_images(i).name])
    hold on
end
hold off

%% Auto peak finder all images analysed one strip

    All_peaks = [];

    figure
    for i = 1:length(Directory_images)
        RGB = imread(Directory_images(i).name);
        RGB_Red = RGB(:,:,1);
        % Peak 1 right of control
        x=1:length(Peak_and_Trough_Locations(2).StripRange);
        y=RGB_Red(Row_obs,Peak_and_Trough_Locations(2).StripRange);
        subplot(ax(length(Directory_images)),bx(length(Directory_images)),i)
        Measuredpeaks=findpeaksG(x,y,.12,2,2,3,3); 
        if Correct_data_loaded == 0
            if i == 1
                peak_locations_auto = Measuredpeaks(:,2);
            end
            All_peaks = [All_peaks; Measuredpeaks];
        end
        plot(x,y,'r');
        title([Directory_images(i).name])
        text(Measuredpeaks(:,2),Measuredpeaks(:,3),num2str(Measuredpeaks(:,1)))
        hold on
        vline(peak_locations_auto)
    end

    hold off

%% Manual peak find

% Visual check peak find is correct
figure
for k = 1:length(Peak_and_Trough_Locations)
    subplot(2,2,k)
    RGB = imread(Directory_images(1).name);
    RGB_Red = RGB(:,:,1);
    % Peak 1 right of control
    x=1:length(Peak_and_Trough_Locations(k).StripRange);
    y=RGB_Red(Row_obs,Peak_and_Trough_Locations(k).StripRange);
    Measuredpeaks=findpeaksG(x,y,.12,2,2,3,3); 
    if Correct_data_loaded == 0
        if i == 1
            peak_locations_auto = Measuredpeaks(:,2);
        end
        All_peaks = [All_peaks; Measuredpeaks];
    end
    plot(x,y,'r');
    title([Directory_images(1).name,'. Strip number: ',num2str(k)])
    text(Measuredpeaks(:,2),Measuredpeaks(:,3),num2str(Measuredpeaks(:,1)))

    hold on
    vline(peak_locations_auto)
    hold off
    if Correct_data_loaded == 0
        Peak_and_Trough_Locations(k).Measuredpeaks = peak_locations_auto;
    end
end

try
sgtitle('Peak finder image')
catch
    disp('Second to last figure is for finding peaks.')
end

figure
for k = 1:length(Peak_and_Trough_Locations)
    subplot(2,2,k)
    RGB = imread(Directory_images(end).name);
    RGB_Red = RGB(:,:,1);
    % Peak 1 right of control
    x=1:length(Peak_and_Trough_Locations(k).StripRange);
    y=RGB_Red(Row_obs,Peak_and_Trough_Locations(k).StripRange);
    Measuredpeaks=findpeaksG(x,y,.12,2,2,3,3); 
    if Correct_data_loaded == 0
        if i == 1
            peak_locations_auto = Measuredpeaks(:,2);
        end
        All_peaks = [All_peaks; Measuredpeaks];
    end
    plot(x,y,'r');
    title([Directory_images(end).name,'. Strip number: ',num2str(k)])
    text(Measuredpeaks(:,2),Measuredpeaks(:,3),num2str(Measuredpeaks(:,1)))

    hold on
    vline(peak_locations_auto)
    hold off
    if Correct_data_loaded == 0
        Peak_and_Trough_Locations(k).Measuredpeaks = peak_locations_auto;
    end
end

try
sgtitle('Trough finder image')
catch
    disp('Last figure is for finding peaks.')
end

% clc
disp('Correct any wrong peak locations by opening "S" and adjusting S(n).Measuredpeaks')
disp('Get first 10 correct then run: Completion_analysis')
open('Peak_and_Trough_Locations')






















