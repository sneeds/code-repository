%% Change peak find location or remove extra peaks found

Peak_and_Trough_Locations(1).All_images_peak_intensities = [];
Peak_and_Trough_Locations(1).Troughs = [];

for k = 1:length(Peak_and_Trough_Locations)

abba = zeros(length(Peak_and_Trough_Locations(k).Measuredpeaks),2);
abba(:,1) = 1:length(Peak_and_Trough_Locations(k).Measuredpeaks);
abba(:,2) = Peak_and_Trough_Locations(k).Measuredpeaks;
peak_locations_manual = abba;

% % Remove peaks
% remove_peaks = input('Remove any auto-found peaks before first real peak? Input 0 or 1: ');
% if remove_peaks == 1
%     Number_of_peaks_to_remove = input('Number of peaks to remove before first point e.g.[1,2,3]; : ');
%     peak_locations_manual(Number_of_peaks_to_remove,:) = [];
% end

% Auto set peaks to 10
if length(peak_locations_manual)>10
    peak_locations_manual(11:end,:) = [];
end

peak_locations_manual(:,1) = [];

    All_images_peak_intensities = zeros(length(Directory_images),length(peak_locations_manual));
    All_images_trough_intensities = zeros(length(Directory_images),length(peak_locations_manual)-1);
    
    for i = 1:length(Directory_images)
        RGB = imread(Directory_images(i).name);
        RGB_Red = RGB(:,:,1);
        y=RGB_Red(Row_obs,Peak_and_Trough_Locations(k).StripRange);
        for ii = 1:length(peak_locations_manual)
            All_images_peak_intensities(i,ii) = max(y(peak_locations_manual(ii)-3:peak_locations_manual(ii)+3));
            if ii ~= length(peak_locations_manual)
                All_images_trough_intensities(i,ii) = min(y(peak_locations_manual(ii):peak_locations_manual(ii+3)));
            else
                a = Peak_and_Trough_Locations(k).Measuredpeaks(:);
                b = diff(a);
            end
        end
    end
    Peak_and_Trough_Locations(k).All_images_peak_intensities = All_images_peak_intensities;
    Peak_and_Trough_Locations(k).Troughs = All_images_trough_intensities;
    clc
end

Peak_and_Trough_Locations(1).filename = Directory_images;

save('Image_Analysis_workspace')


% %% Visual plot of intensities
% 
% figure
% for i = 1:length(Peak_and_Trough_Locations) % Number of strips
%     subplot(2,2,i)
%     for ii = 1:length(Directory_images) % Number of images
%         plot(1:10,Peak_and_Trough_Locations(i).All_images_peak_intensities(:,ii),'-x')
%         hold on
%     end
%     title([Directory_images(1).name,'. Strip number: ',num2str(i)])
%     xlabel('Capillary Number')
%     ylabel('Intensity')
% end
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    

